<h2 align="center">About Me</h2>
<p>Hi 👋, I'm James, a fourth-year computer science student at Simon Fraser University with a passion for Software Development Engineering in Test (SDET).</p>
<p>My experience includes working as an SDET Intern at Atimi Software, a leader in custom mobile app design and development, serving industry giants like Amazon, Bloomberg, and Best Buy. During my 8-month internship, I successfully automated 84% of manual testing processes, significantly increasing efficiency and coverage. This role also provided valuable exposure to quality assurance methodologies, test automation frameworks, and tools that ensure software reliability and performance.</p>
<p>My technical strengths lie in developing robust automated test scripts, implementing continuous integration/continuous deployment (CI/CD) pipelines, and working with testing tools such as Selenium, JUnit, and TestNG. I thrive in environments where I can enhance software quality, advocate for best testing practices, and collaborate closely with development teams to identify and resolve issues early in the development lifecycle.</p>
<p>I'm on a mission to secure a role in SDET, leveraging my experience in test automation and my ability to work effectively within agile teams. Let's connect and explore opportunities to enhance software quality, streamline testing processes, and ensure the delivery of reliable and high-performing applications together!</p>

<!-- Projects -->

<!-- <h2 align="center">My Professional Journey</h2>

<details>
  <summary>:briefcase: Projects</summary>

  ### 🌊 [Wavee (Group Project)](https://gitlab.com/hojungpark/wavee)
  In the Wavee project, I took full charge in constructing a efficient Cloud Architecture, configuring Docker and various products of GCP. Additionally, I have managed to reduce the cloud build time by 40% using caching and parallel execution.
</details>
<details>
  <summary>:scroll: Certifications</summary>
  
  ### AWS SysOps Admin (In progress)
</details> -->